﻿#include <time.h>

#include "Terrain.h"


Terrain::Terrain( const unsigned int &width, const unsigned int &height )
: _width( width ), _height( height )
{
    initialize();
}


void Terrain::initialize()
{
    int seed = (int) time( NULL );

    module::RidgedMulti mountains;
    mountains.SetSeed( seed );

    module::Billow hills;
    hills.SetSeed( seed );
    hills.SetFrequency(2.0);

    module::ScaleBias flatland;
    flatland.SetSourceModule(0, hills);
    flatland.SetScale(0.125);
    flatland.SetBias(-0.75);

    module::Perlin terrain_map;
    terrain_map.SetSeed( seed );
    terrain_map.SetFrequency(0.5);
    terrain_map.SetPersistence(0.25);

    module::Select joined;
    joined.SetSourceModule(0, flatland);
    joined.SetSourceModule(1, mountains);
    joined.SetControlModule(terrain_map);
    joined.SetBounds(0.0, 1000.0);
    joined.SetEdgeFalloff(0.125);

    module::Turbulence terrain;
    terrain.SetSeed( seed );
    terrain.SetSourceModule (0, joined);
    terrain.SetFrequency (4.0);
    terrain.SetPower (0.125);

    module::ScaleBias normalizer;
    normalizer.SetSourceModule(0, terrain);
    normalizer.SetScale(0.5);
    normalizer.SetBias(0.5);

    utils::NoiseMap height_map;
    utils::NoiseMapBuilderSphere height_map_builder;
    height_map_builder.SetSourceModule( normalizer );
    height_map_builder.SetDestNoiseMap( height_map );
    height_map_builder.SetDestSize( _width, _height );
    height_map_builder.SetBounds( -90.0, 90.0, -180.0, 180.0 );
    height_map_builder.Build();
    utils::Image image;
    utils::RendererImage renderer;
    renderer.SetSourceNoiseMap(height_map);
    renderer.SetDestImage(image);
    renderer.Render();

    //utils::WriterBMP writer;
    //writer.SetSourceImage (image);
    //writer.SetDestFilename ("tutorial.bmp");
    //writer.WriteDestFile ();


    int width = image.GetWidth();
    int height = image.GetHeight();
    _qimage = QImage(width, height,QImage::Format_ARGB32);

    for (int y = 0; y < height; y++) {
        utils::Color* color = image.GetSlabPtr(y);

        for (int x = 0; x < width; x++) {
            QColor qcolor(color->red, color->green, color->blue);
            _qimage.setPixel(x,y, qcolor.rgba());
            ++color;
        }
    }
}

QImage* Terrain::height_map() {
    return &_qimage;
}


int Terrain::width()
{
    return _width;
}


int Terrain::height()
{
    return _height;
}


float Terrain::level(const unsigned int x, const unsigned int y)
{
    // Coordinates must actually be inside the terrain!
    assert( x < _width );
    assert( y < _height );


    QColor color = _qimage.pixel(x, y);
    return ( color.red() / 255.0 );
}


void Terrain::setMaterial(const unsigned int x, const unsigned int y)
{
    // How distant we are from the equator
    float cold = fabs( 2.0 * ( (float) y / (float) _height - 0.5) );
    cold *= cold * cold * cold;
    const Color3d snow ( 0.8, 0.8, 1.0 );
    const Color3d land ( 0.33, 0.25, 0.16 );
    Color3d ambient = cold * snow + (1 - cold) * land;

    GLfloat mat_emission[] = {0.0, 0.0, 0.0, 0.0};
    GLfloat mat_ambient[] = { ambient.r(), ambient.g(), ambient.b(), 1.0 };
    ambient *= 0.2;
    GLfloat mat_diffuse[] = { 0.75, 0.61, 0.23, 1.0 };
    GLfloat mat_specular[] = { ambient.r(), ambient.g(), ambient.b(), 1.0 };
    GLfloat shininess = 10.0;

    glMaterialfv( GL_FRONT, GL_EMISSION, mat_emission );
    glMaterialfv( GL_FRONT, GL_AMBIENT, mat_ambient );
    glMaterialfv( GL_FRONT, GL_DIFFUSE, mat_diffuse );
    glMaterialfv( GL_FRONT, GL_SPECULAR, mat_specular );
    glMaterialf( GL_FRONT, GL_SHININESS, shininess );
}


Point2d Terrain::to_cartesian(const float longitude, const float latitude)
{
    const int x = (int) ( _height_map.GetWidth() * longitude / (2 * PI) );
    const int y = (int) ( _height_map.GetHeight() * latitude / PI );

    return Point2d (x % _height_map.GetWidth(), y % _height_map.GetHeight());
}



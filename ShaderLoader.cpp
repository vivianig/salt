#include "ShaderLoader.h"
#include <string>
#include <iostream>
#include <fstream>
#include <vector>
#include <algorithm>



std::string ShaderGroup::__readFile(const char *filePath) {
    std::string content;
    std::ifstream fileStream(filePath, std::ios::in);

    if(!fileStream.is_open()) {
	std::cerr << "Could not read file " << filePath << ". File does not exist." << std::endl;
        return "";
    }

    std::string line = "";
    while(!fileStream.eof()) {
	std::getline(fileStream, line);
        content.append(line + "\n");
    }

    fileStream.close();
    return content;
}

void ShaderGroup::loadShader(const char *vertex_path, const char *fragment_path) {
    vertexShaderName = vertex_path;
    fragmentShaderName = fragment_path;
    vertexShader = glCreateShaderObjectARB(GL_VERTEX_SHADER);
    fragmentShader = glCreateShaderObjectARB(GL_FRAGMENT_SHADER);

    std::string vertShaderStr = __readFile(vertex_path);
    std::string fragShaderStr = __readFile(fragment_path);

    const char *vertShaderSrc = vertShaderStr.c_str();
    const char *fragShaderSrc = fragShaderStr.c_str();

    glShaderSourceARB(vertexShader, 1, &vertShaderSrc, NULL);
    glShaderSourceARB(fragmentShader, 1, &fragShaderSrc, NULL);

    glCompileShaderARB(vertexShader);
    glCompileShaderARB(fragmentShader);

    program = glCreateProgramObjectARB();

    glAttachObjectARB(program, vertexShader);
    glAttachObjectARB(program, fragmentShader);

    glLinkProgramARB(program);

    glUseProgramObjectARB(program);
    assert(glGetError()==0);
}

void ShaderGroup::recompileShaders() {
    std::string vertShaderStr = __readFile(vertexShaderName);
    std::string fragShaderStr = __readFile(fragmentShaderName);
    
    const char *vertShaderSrc = vertShaderStr.c_str();
    const char *fragShaderSrc = fragShaderStr.c_str();
    
    glShaderSourceARB(vertexShader, 1, &vertShaderSrc, NULL);
    glShaderSourceARB(fragmentShader, 1, &fragShaderSrc, NULL);
    
    glCompileShaderARB(vertexShader);
    glCompileShaderARB(fragmentShader);
   
    glLinkProgramARB(program);
}

void ShaderGroup::loadHeightMap(QImage* image) {
    // Load texture file
    *image = QGLWidget::convertToGLFormat(*image);
    // Copy file to OpenGL
    glEnable(GL_TEXTURE_2D);
    glActiveTexture(GL_TEXTURE0);
    glGenTextures(1, &textureID);
    glBindTexture(GL_TEXTURE_2D, textureID);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, image->width(), image->height(), 0,
            GL_RGBA, GL_UNSIGNED_BYTE, image->bits());
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    //glBindTexture(GL_TEXTURE_2D,0);
    assert(glGetError()==0);
    _image = image;
}

void ShaderGroup::reloadHeightMap() {
    glDeleteTextures(1,&textureID);
    glBindTexture(GL_TEXTURE_2D,0);
    glActiveTexture(GL_TEXTURE0);
    glGenTextures(1, &textureID);
    glBindTexture(GL_TEXTURE_2D, textureID);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, _image->width(), _image->height(), 0,
            GL_RGBA, GL_UNSIGNED_BYTE, _image->bits());
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
}

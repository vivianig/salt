﻿#include "OrbitingCamera.h"

#ifdef __APPLE__
#include <OpenGL/glu.h>
#else
#include <GL/glu.h>
#endif
#include <math.h>


OrbitingCamera::OrbitingCamera(const Planet &planet)
:   _mov_step( PI / 2.0 ), _turn_step( PI / 2.0 ), _zoom_step( 0.03 ),
    _eye( 0, 180, 0 ), _center( 0, 0, 0 ), _up( 1, 0, 0 ),
    _planet( planet ), _orbiting_factor( 4.0 ),
    _zoom_level( 1.0 ), _zoom_range( 5.0 )
{
    _zooming = 1.0;
    _update_zoom();
    _zooming = 0.0;
}


Point3d OrbitingCamera::_mult(const float * mat, Point3d &vec)
{
    return Point3d(
            mat[0] * vec[0] + mat[4] * vec[1] + mat[8] * vec[2],
            mat[1] * vec[0] + mat[5] * vec[1] + mat[9] * vec[2],
            mat[2] * vec[0] + mat[6] * vec[1] + mat[10] * vec[2]
            );
}


void OrbitingCamera::_update_vectors(float matrix[])
{
    _eye = _mult( matrix, _eye );

    _up = _mult( matrix, _up ).normalized();
}


void OrbitingCamera::_update_move()
{
    // Compute rotation axis (perpendicular to view and up vectors)
    Point3d r_axis = ( _center - _eye ) ^ _up;

    // Save current matrix and create a fresh new one
    glPushMatrix();
    glLoadIdentity();

    // Get transformation matrix from OpenGl
    glTranslatef( _eye.x(), _eye.y(), _eye.z() );
    glRotatef( _moving * _mov_step, r_axis.x(), r_axis.y(), r_axis.z() );
    float rotation[16];
    glGetFloatv( GL_MODELVIEW_MATRIX, rotation );

    // Reset previous matrix
    glPopMatrix();

    _update_vectors( rotation );
}


void OrbitingCamera::_update_strafe()
{
    // Compute rotation axis (up vector)
    Point3d r_axis = _up;

    // Save current matrix and create a fresh new one
    glPushMatrix();
    glLoadIdentity();

    // Get transformation matrix from OpenGl
    glTranslatef( _eye.x(), _eye.y(), _eye.z() );
    glRotatef( _strafing * _mov_step, r_axis.x(), r_axis.y(), r_axis.z() );
    float rotation[16];
    glGetFloatv( GL_MODELVIEW_MATRIX, rotation );

    // Reset previous matrix
    glPopMatrix();

    _update_vectors( rotation );
}


void OrbitingCamera::_update_zoom()
{
    _zoom_level += _zooming * _zoom_step;

    if ( _zoom_level < 0.0 )
        _zoom_level = 0.0;

    else if (_zoom_level > 1.0 )
        _zoom_level = 1.0;

    _eye.normalize();
    _eye *= _planet.scale();
    _eye *= _zoom_level * _zoom_range + _orbiting_factor;
}


void OrbitingCamera::move(const float flag)
{
    assert( flag == -1.0 || flag == 0.0 || flag == 1.0 );

    _moving = flag;
}


void OrbitingCamera::strafe(const float flag)
{
    assert( flag == -1.0 || flag == 0.0 || flag == 1.0 );

    _strafing = flag;
}


void OrbitingCamera::zoom(const float flag)
{
    assert( flag == -1.0 || flag == 0.0 || flag == 1.0 );

    _zooming = flag;
}


void OrbitingCamera::look()
{
    if ( _zooming != 0.0 )
        _update_zoom();

    if ( _moving != 0.0 )
        _update_move();

    if ( _strafing != 0.0 )
        _update_strafe();

    gluLookAt(
            _eye.x(), _eye.y(), _eye.z(),
            _center.x(), _center.y(), _center.z(),
            _up.x(), _up.y(), _up.z()
            );
}

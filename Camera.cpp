﻿#ifdef __APPLE__
#include <OpenGL/glu.h>
#else
#include <GL/glu.h>
#endif
#include <math.h>

#include "Camera.h"


Camera::Camera(const Planet &planet)
:   _mov_step( PI / 16.0 ), _turn_step( PI / 8.0 ),
    _eye( 0, 1.0, 0 ), _center( 100.0, 100.0, 0 ), _up( 0, 1.0, 0 ),
    _planet( planet )
{
    _eye.normalize();
    Point2d eye_s = _planet.to_spherical( _eye.normalized() );
    _eye *= ( 1.0 + _planet.height_at(eye_s.x(), eye_s.y()) );
    _eye *= _planet.scale();
}


Point3d Camera::_mult(const float * mat, Point3d &vec)
{
    return Point3d(
            mat[0] * vec[0] + mat[4] * vec[1] + mat[8] * vec[2],
            mat[1] * vec[0] + mat[5] * vec[1] + mat[9] * vec[2],
            mat[2] * vec[0] + mat[6] * vec[1] + mat[10] * vec[2]
            );
}


void Camera::_update_vectors(float matrix[])
{
    Point2d eye_s = _planet.to_spherical( _eye.normalized() );
    _eye = _mult( matrix, _eye ).normalized();
    _eye *= ( 1.0 + _planet.height_at(eye_s.x(), eye_s.y()) );
    _eye *= _planet.scale();

    _center = _mult( matrix, _center );
    _up = _mult( matrix, _up ).normalized();
}


void Camera::_update_move()
{
    // Compute rotation axis (perpendicular to planet radius and view vector)
    Point3d r_axis = ( _center - _eye ) ^ _eye;

    // Save current matrix and create a fresh new one
    glPushMatrix();
    glLoadIdentity();

    // Get transformation matrix from OpenGl
    glRotatef( _moving * _mov_step, r_axis.x(), r_axis.y(), r_axis.z() );
    float rotation[16];
    glGetFloatv( GL_MODELVIEW_MATRIX, rotation );

    // Reset previous matrix
    glPopMatrix();

    _update_vectors( rotation );
}


void Camera::_update_strafe()
{
    // Compute rotation axis (perpendicular to planet radius, parallel to view)
    Point3d r_axis = (( _center - _eye ) ^ _eye) ^ _eye;

    // Save current matrix and create a fresh new one
    glPushMatrix();
    glLoadIdentity();

    // Get transformation matrix from OpenGl
    glRotatef( _strafing * _mov_step, r_axis.x(), r_axis.y(), r_axis.z() );
    float rotation[16];
    glGetFloatv( GL_MODELVIEW_MATRIX, rotation );

    // Reset previous matrix
    glPopMatrix();

    _update_vectors( rotation );
}


void Camera::_update_turn()
{
   // Save current matrix and create a fresh new one
    glPushMatrix();
    glLoadIdentity();

    // Get transformation matrix from OpenGl
    glRotatef( _turning * _turn_step, _eye.x(), _eye.y(), _eye.z() );
    float rotation[16];
    glGetFloatv(GL_MODELVIEW_MATRIX, rotation);

    // Reset previous matrix
    glPopMatrix();

    // Update point of view
    _center = _mult( rotation, _center );
    _up = _mult( rotation, _up ).normalized();
}


void Camera::_update_pitch()
{
    // Compute rotation axis (perpendicular to planet radius and view vector)
    Point3d r_axis = ( _center - _eye ) ^ _up;

    // Save current matrix and create a fresh new one
    glPushMatrix();
    glLoadIdentity();

    // Get transformation matrix from OpenGl
    glRotatef( _pitching * _turn_step, r_axis.x(), r_axis.y(), r_axis.z() );
    float rotation[16];
    glGetFloatv(GL_MODELVIEW_MATRIX, rotation);

    // Reset previous matrix
    glPopMatrix();

    // Update point of view
    _center = _mult( rotation, _center );
    _up = _mult( rotation, _up ).normalized();
}


void Camera::move(const float flag)
{
    assert( flag == -1.0 || flag == 0.0 || flag == 1.0 );

    _moving = flag;
}


void Camera::strafe(const float flag)
{
    assert( flag == -1.0 || flag == 0.0 || flag == 1.0 );

    _strafing = flag;
}


void Camera::turn(const float flag)
{
    assert( flag == -1.0 || flag == 0.0 || flag == 1.0 );

    _turning = flag;
}


void Camera::pitch(const float flag)
{
    assert( flag == -1.0 || flag == 0.0 || flag == 1.0 );

    _pitching = flag;
}


void Camera::look()
{
    if ( _moving != 0.0 )
        _update_move();

    if ( _strafing != 0.0 )
        _update_strafe();

    if ( _turning != 0.0 )
        _update_turn();

    if ( _pitching != 0.0 )
        _update_pitch();

    gluLookAt(
            _eye.x(), _eye.y(), _eye.z(),
            _center.x(), _center.y(), _center.z(),
            _up.x(), _up.y(), _up.z()
            );
}



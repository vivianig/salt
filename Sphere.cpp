// From http://www.glprogramming.com/red/chapter02.html#name8
#ifdef __APPLE__
#include <OpenGL/glu.h>
#else
#include <GL/glu.h>
#endif
#include <stdlib.h>
#include <math.h>
#include <iostream>

#include "Sphere.h"
#include "Point3.h"

using std::cout;
using std::endl;

// coordinates to have all vertices at dist 1 fomr center
#define X 0.525731112119133606
#define Z 0.850650808352039932

// icosahedron vertices
Point3d Sphere::vertices[12] = {
    Point3d(-X, 0.0, Z), Point3d(X, 0.0, Z), Point3d(-X, 0.0, -Z), Point3d(X, 0.0, -Z),
    Point3d(0.0, Z, X), Point3d(0.0, Z, -X), Point3d(0.0, -Z, X), Point3d(0.0, -Z, -X),
    Point3d(Z, X, 0.0), Point3d(-Z, X, 0.0), Point3d(Z, -X, 0.0), Point3d(-Z, -X, 0.0)
    /*Point3d(-X, 0.0, Z), Point3d(X, 0.0, Z), Point3d(-X, 0.0, -Z), Point3d(X, 0.0, -Z),
    Point3d(0.0, Z, X), Point3d(0.0, Z, -X), Point3d(0.0, -Z, X), Point3d(0.0, -Z, -X),
    Point3d(Z, X, 0.0), Point3d(-Z, X, 0.0), Point3d(Z, -X, 0.0), Point3d(-Z, -X, 0.0)*/
};

// icosehedron faces
int Sphere::triangle_indexes[20][3] = {
    {0,4,1}, {0,9,4}, {9,5,4}, {4,5,8}, {4,8,1},
   {8,10,1}, {8,3,10}, {5,3,8}, {5,2,3}, {2,7,3},
   {7,10,3}, {7,6,10}, {7,11,6}, {11,0,6}, {0,1,6},
   {6,1,10}, {9,0,11}, {9,11,2}, {9,2,5}, {7,2,11}
    /*{1,4,0}, {4,9,0}, {4,5,9}, {8,5,4}, {1,8,4},
    {1,10,8}, {10,3,8}, {8,3,5}, {3,2,5}, {3,7,2},
    {3,10,7}, {10,6,7}, {6,11,7}, {6,0,11}, {6,1,0},
    {10,1,6}, {11,0,9}, {2,11,9}, {5,2,9}, {11,2,7}*/
};

Sphere::Sphere(const int divs)
:_angle(0.0), _subdiv(divs), _animation(true)
{
    ;
}




// Subdivide each face by depth
void Sphere::subdivide(Point3d p1, Point3d p2, Point3d p3, int depth)
{
    Point3d p12, p23, p31;
    int i;

    if (depth == 0) {
        p1.normalize();
        p2.normalize();
        p3.normalize();

        glBegin(GL_TRIANGLES);
        glVertex3d(p1.x(), p1.y(), p1.z());
        glVertex3d(p2.x(), p2.y(), p2.z());
        glVertex3d(p3.x(), p3.y(), p3.z());
        glEnd();

        glBegin(GL_LINE_LOOP);
        glVertex3d(p1.x(), p1.y(), p1.z());
        glVertex3d(p2.x(), p2.y(), p2.z());
        glVertex3d(p3.x(), p3.y(), p3.z());
        glEnd();
    } else
    {
        // midpoint of face
        for (i = 0; i < 3; i++)
        {
            p12[i] = (p1[i]+p2[i])/2.0;
            p23[i] = (p2[i]+p3[i])/2.0;
            p31[i] = (p3[i]+p1[i])/2.0;
        }


        // lift midpoints on the sphere
        p12.normalize();
        p23.normalize();
        p31.normalize();

        // subdivide triangle into 4
        subdivide(p1, p12, p31, depth-1);
        subdivide(p2, p23, p12, depth-1);
        subdivide(p3, p31, p23, depth-1);
        subdivide(p12, p23, p31, depth-1);
    }
}

//Draw the sphere
void Sphere::display(void)
{
    int i;
    glPolygonMode( GL_FRONT_AND_BACK, GL_LINE );


    if (_animation) _angle+=0.3;
    if (_angle>360) _angle-=360.0;

    glPushMatrix();
    //glRotatef(_angle,0,1,0);

    // subdivide each face of the triangle
    for (i = 0; i < 20; i++)
    {
        subdivide(
            Sphere::vertices[Sphere::triangle_indexes[i][0]],
            Sphere::vertices[Sphere::triangle_indexes[i][1]],
            Sphere::vertices[Sphere::triangle_indexes[i][2]],
            _subdiv
        );
    }

    glPopMatrix();
    glFlush();
}

// Glut BS: not used
void Sphere::reshape(int w, int h)
{
    glViewport(0, 0, w, h);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glOrtho(-1.25, 1.25, -1.25 , 1.25 , -2.0, 2.0);
    glMatrixMode(GL_MODELVIEW);
}

// Glut BS: not used
void Sphere::idle()
{
    if (_animation) _subdiv=(int)(_angle/70);

    //glutPostRedisplay();
}
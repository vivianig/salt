uniform sampler2D Texture;
varying vec2 vUv;
const float magnitude = 0.5;

varying vec3 normal, lightDir, eyeVec;

void main()
{
    vUv = vec2(gl_MultiTexCoord0);

    vec3 vVertex = vec3(gl_ModelViewMatrix * gl_Vertex);

    lightDir = vec3(gl_LightSource[0].position.xyz);
    eyeVec = -vVertex;

    // calculate height map from brightness of texture sample
    vec4 pixel = texture2D(Texture, vUv);
	
	float here = pixel.x;
	float right = texture2D(Texture, vec2(vUv.x + 10.0, vUv.y) ).x;
	float up = texture2D(Texture, vec2(vUv.x, vUv.y + 10.0) ).x;

	vec3 va = normalize( vec3(1.0, 0.0, here - right) );
	vec3 vb = normalize( vec3(0.0, 1.0, here - up) );

    float dv = (pixel.r + pixel.g + pixel.b)/3.0;
    dv = dv * magnitude;

    // displace
    vec4 displace = vec4(gl_Normal * dv, 0.0);
    gl_Position = gl_ModelViewProjectionMatrix * (gl_Vertex + displace);
	normal = normalize(gl_Normal - cross(va, vb).y);
}

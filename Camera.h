﻿#ifndef _CAMERA_H
#define _CAMERA_H

#include "Point3.h"
#include "Planet.h"

class Camera {

    private:
        /** The step by which the camera moves. */
        float _mov_step;

        /** The step by which the camera turns. */
        float _turn_step;

        /** The eye vector (camera position). */
        Point3d _eye;

        /** The center of attention. */
        Point3d _center;

        /** Up vector (to the sky). */
        Point3d _up;

        /** Tells whether the camera is moving.
         *
         *  Set to -1, 0 or 1 when the camera is moving backwards, not moving
         *  or is moving forwards.
         */
        float _moving;

        /** Tells whether the camera is strafing (left or right).
         *
         *  Set to -1, 0 or 1 when the camera is strafing left, not strafing or
         *  is strafing right.
         */
        float _strafing;

        /** Tells whether the camera is strafing (left or right).
         *
         *  Set to -1, 0 or 1 when the camera is turning left, not turning or
         *  is turning right.
         */
        float _turning;

        /** Tells whether the camera is pitching (up or down).
         *
         *  Set to -1, 0 or 1 when the camera is turning left, not turning or
         *  is turning right.
         */
        float _pitching;

        /** The planet the camera is walking on. */
        Planet _planet;

        /** Multiplication between 4x4 column major matrix and a 3x1 vector. */
        Point3d _mult(const float *mat, Point3d &vec);

        /** Updates the 3 camera vectors with the given matrix. */
        void _update_vectors(float matrix[]);

        /** Moves the camera vectors. */
        void _update_move();

        /** Strafes the camera vectors. */
        void _update_strafe();

        /** Turns the camera vectors. */
        void _update_turn();

        /** Pitches the camera vectors. */
        void _update_pitch();

    public:
        /** Constructor for the camera. */
        Camera(const Planet &planet);

        /** Sets the movement flag (-1 is forwards, 0 stop, 1 backwards). */
        void move(const float flag);

        /** Sets the strafe flag (-1 is left, 0 stop, 1 right). */
        void strafe(const float flag);

        /** Sets the strafe flag (-1 is right, 0 stop, 1 left). */
        void turn(const float flag);

        /** Sets the pitch flag (-1 is right, 0 stop, 1 left). */
        void pitch(const float flag);

        /** Checks the flags for updates and positions the camera. */
        void look();
};

#endif // _CAMERA_H


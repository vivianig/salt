#ifndef SHADERLOADER_H
#ifdef __APPLE__
#include <OpenGL/glu.h>
#else
#define GL_GLEXT_PROTOTYPES
#include <GL/glu.h>
#include <GL/glcorearb.h>
#endif
#include "Base.h"
#include <QtOpenGL>
#define SHADERLOADER_H
class ShaderGroup {
 public:
    GLuint textureID;
    GLhandleARB program;
    
 ShaderGroup(): textureID(0){};

    void loadShader(const char *vertex_path, const char *fragment_path);    
    void loadHeightMap(QImage* qimage);
    void reloadHeightMap();
    void recompileShaders();
 private:
    QImage* _image;
    std::string __readFile(const char *filePath);
    const char * vertexShaderName;
    const char * fragmentShaderName;
    GLhandleARB vertexShader;
    GLhandleARB fragmentShader;
};

#endif // SHADERLOADER_H

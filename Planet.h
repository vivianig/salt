﻿#ifndef PLANET_H
#define PLANET_H

#include <QtOpenGL>
#include "Point3.h"
#include "Point2.h"
#include "utils/noiseutils.h"
#include "Terrain.h"

class Planet
{
    private:
        typedef std::vector<Point3d> PointArray;
        typedef std::vector<Point2d> TextureArray;

        /** The number of latitude steps. */
        const int _lats;

        /** The number of longitude steps. */
        const int _longs;

        /** The factor by which the planet is scaled. */
        int _scale;

        /** The terrain the of the planet. */
        Terrain _terrain;

        /** The factor by which the height-map deforms the sphere. */
        const float _deformation;

        /** Vector containing the strips which compose the planet. */
        std::vector<PointArray> strips;

        /** Vector containing the texture coordinates of the planet. */
        std::vector<TextureArray> textures;

        /** Vector containing the normal for the triangles. */
        std::vector<PointArray> normals;

        /** Builds the segments which compose the planet. */
        void build();


        /**
         * Build a segment (strip) of triangles from pole to pole.
         *
         * @param index   : The index of the strip (determines the position).
         * @param phiStep : The width of the strip (in radians).
         * @param hmap    : The height-map to apply to the planet.
         */
        void buildSegment(const int &index, const float &phiStep);


        /**
         * Sets the given vertices and texture coordinates in their respective
         * vectors.
         *
         * @param point      : The vertex to set.
         * @param txt_coords : The texture coordinate to set.
         * @param segment    : The segment vector to which the vertex belongs
         * @param txt        : The vector of texture coordinates.
         */
        void set(const Point3d &p, const Point2d &t, PointArray &segment,
                TextureArray &txt, PointArray &seg_normals) const;

    public:
        /**
         * Constructor for a planet with the given latitude and longitude steps.
         *
         * @param lats  : The number of latitude steps.
         * @param longs : The number of longitude steps.
         */
        Planet(const int &lats=20, const int &longs=10, const int &scale=100);


        /**
         * Returns the image of the heightmap in gl format.
         *
         * @return The image in gl format.
         */
        QImage* height_map();


        /**
         * Returns the height of the terrain at the given coordinates.
         *
         * @param x : The x coordinate.
         * @param y : The y coordinate.
         *
         * @return 
         */
        float height_at(const float x, const float y);


        /** Returns the scale of the planet. */
        int scale() const;


        /** Draws the planet. */
        void draw();


        /** Converts a point from cartesian to spherical coordinates. */
        Point2d to_spherical(const Point3d point);
};

#endif // PLANET_H

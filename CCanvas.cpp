#include "ShaderLoader.h"
#include <noise/noise.h>

#include "utils/noiseutils.h"
#include "CCanvas.h"
#include "Base.h"
#include "Planet.h"
#include "Camera.h"
#include "OrbitingCamera.h"

#include "Sphere.h"

using namespace std;
Sphere sphere = Sphere(0);
Planet planet = Planet(300, 200, 100);
Camera camera = Camera(planet);
OrbitingCamera o_camera = OrbitingCamera( planet );
bool orbiting = true;
bool rotate_sphere = false;
bool fullscreen = false;

void CCanvas::initializeGL()
{
    glClearColor(0.0666f, 0.1019f, 0.1333f, 1.0f);
    // Depth Buffer Setup
    glClearDepth(1.0f);
    // Enables Depth Testing
    glEnable(GL_DEPTH_TEST);
    // The Type Of Depth Testing To Do
    glDepthFunc(GL_LEQUAL);
    // Really Nice Perspective Calculations
    glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);

    GLfloat ambient[] = { 0.3, 0.2, 0.3, 1.0 };
    GLfloat diffuse[] = { 0.85, 0.65, 0.40, 0.8 };
    GLfloat specular[] = { 1.0, 1.0, 0.60, 1.0 };
    GLfloat position[] = { -80.0, 40.0, 0.0, 1.0 };

    glShadeModel( GL_SMOOTH );

    glLightfv( GL_LIGHT0, GL_AMBIENT, ambient );
    glLightfv( GL_LIGHT0, GL_DIFFUSE, diffuse );
    glLightfv( GL_LIGHT0, GL_SPECULAR, specular );
    glLightfv( GL_LIGHT0, GL_POSITION, position );

    glEnable( GL_LIGHTING );
    glEnable( GL_LIGHT0 );

    std::cout<<"Loading Shader"<<std::endl;
    shaderGroup.loadShader(vertexShader, fragmentShader);
    std::cout<<"Shader Loaded"<<std::endl<<"Loading texture"<<std::endl;
    shaderGroup.loadHeightMap(planet.height_map());
    std::cout<<"Texture loaded"<<std::endl;
}


void CCanvas::resizeGL(int width, int height)
{
    // set up the window-to-viewport transformation
    glViewport( 0,0, width,height );

    // vertical camera opening angle
    double beta = 20.0;

    // aspect ratio
    double gamma;
    if (height > 0)
        gamma = width/(double)height;
    else
        gamma = width;

    // front and back clipping plane at
    double n = -0.1;
    double f = -1000.0;

    // frustum corners
    double t = -tan(beta*3.14159/360.0) * n;
    double b = -t;
    double r = gamma * t;
    double l = -r;

    // set projection matrix
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glFrustum( l,r , b,t , -n,-f );

    //this->setCursor(Qt::CrossCursor);
}


void CCanvas::paintGL()
{
    // clear screen and depth buffer
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glBindTexture(GL_TEXTURE_2D, shaderGroup.textureID);

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    if ( orbiting )
        o_camera.look();
    else
        camera.look();

    // Increase animation parameter
    tau += 1.0;

    // Save current state
    glPushMatrix();

    // Apply rotation to light
    glLoadIdentity();
    glRotatef( tau / 8, 0, 1, 0);
    GLfloat position[] = { -80.0, 40.0, -30.0, 1.0 };
    glLightfv( GL_LIGHT0, GL_POSITION, position );

    // Reset state
    glPopMatrix();

    glUseProgramObjectARB(shaderGroup.program);

    // Rotate program only when orbiting and when th flag is on
    if ( rotate_sphere && orbiting )
        glRotatef( tau / 2, 0, 1, 0);

    planet.draw();

}


void CCanvas::keyPressEvent(QKeyEvent *k)
{
    if ( k->isAutoRepeat() )
        return;

    switch( k->key() )
    {
        case Qt::Key_W:
            orbiting ? o_camera.move( -1.0 ) : camera.move( -1.0 );
            break;

        case Qt::Key_S:
            orbiting ? o_camera.move( 1.0 ) : camera.move( 1.0 );
            break;

        case Qt::Key_A:
            orbiting ? o_camera.strafe( -1.0 ) : camera.strafe( 1.0 );
            break;

        case Qt::Key_D:
            orbiting ? o_camera.strafe( 1.0 ) : camera.strafe( -1.0 );
            break;

        case Qt::Key_Left:
            if ( !orbiting )
                camera.turn( 1.0 );
            break;

        case Qt::Key_Right:
            if ( !orbiting )
                camera.turn( -1.0 );
            break;

        case Qt::Key_Up:
            orbiting ? o_camera.zoom( -1.0 ) : camera.pitch( 1.0 );
            break;

        case Qt::Key_Down:
            orbiting ? o_camera.zoom( 1.0 ) : camera.pitch( -1.0 );
            break;

        case Qt::Key_R:
            rotate_sphere = !rotate_sphere;
            break;

        case Qt::Key_F:
            fullscreen ? window()->showNormal() : window()->showFullScreen();
            fullscreen = !fullscreen;
            break;

        case Qt::Key_Escape:
            exit( EXIT_SUCCESS );
            break;

        case Qt::Key_L:
            int mode;
            glGetIntegerv(GL_POLYGON_MODE, &mode);
            glPolygonMode( GL_FRONT, mode == GL_LINE ? GL_FILL : GL_LINE );
            break;

        case Qt::Key_V:
            orbiting = !orbiting;
            break;

        case Qt::Key_C:
            shaderGroup.recompileShaders();
            break;
    }
}


void CCanvas::keyReleaseEvent(QKeyEvent *k)
{
    if ( k->isAutoRepeat() )
        return;

    switch ( k->key() )
    {
        case Qt::Key_W:
        case Qt::Key_S:
            orbiting ? o_camera.move( 0.0 ) : camera.move( 0.0 );
            break;

        case Qt::Key_A:
        case Qt::Key_D:
            orbiting ? o_camera.strafe( 0.0 ) : camera.strafe( 0.0 );
            break;

        case Qt::Key_Left:
        case Qt::Key_Right:
            if ( !orbiting )
                camera.turn( 0.0 );
            break;

        case Qt::Key_Up:
        case Qt::Key_Down:
            orbiting ? o_camera.zoom( 0.0 ) : camera.pitch( 0.0 );
            break;
    }

}


void CCanvas::mousePressEvent(QMouseEvent *k) {
    int ratio;
    if(k->button()==Qt::LeftButton)
	ratio = 1;
    else if(k->button()==Qt::RightButton)
	ratio = -1;
    else
	return;
    GLint viewport[4];                 
    glGetIntegerv(GL_VIEWPORT, viewport); 
    GLdouble modelview[16]; 
    glGetDoublev(GL_MODELVIEW_MATRIX, modelview);  
    GLdouble projection[16];                
    glGetDoublev(GL_PROJECTION_MATRIX, projection); 
    Point2d mouse;               
    GLfloat winX, winY, winZ;  
    winX = k->x();     
    winY = k->y();
    winY = viewport[3] - winY;
    glReadPixels(winX, winY, 1, 1, GL_DEPTH_COMPONENT, GL_FLOAT, &winZ);
    GLdouble posX, posY, posZ;
    gluUnProject( winX, winY, winZ, modelview, projection, viewport, &posX, &posY, &posZ);
    Point3d point(posX, posY, posZ);
    point.normalize();
    //    debug.push_back(point);
    Point2d coordinates = planet.to_spherical(point);

    _updateHeight_map(ratio, coordinates[0] *512/PI, coordinates[1]*512/PI);

    shaderGroup.reloadHeightMap();
}

double CCanvas::_twoDGaussian(int x, int y) {
    return _gaussianMatrix[x][y];
}

void CCanvas::_updateHeight_map(int ratio, int x, int y) {
    int range = 3;
    int increase = 100;
    QColor color;
    int old_color;
    for(int h=(-range); h<=range;++h){
	for (int k=(-range); k<=range; ++k) {
	    if(y+k <0 || x+h < 0 || x+h >= 1024 || y+k>=512)
		continue;
	    old_color = qRed(planet.height_map()->pixel(x+h,y+k));
	    int delta = ratio*round(increase*_twoDGaussian(h+range, k+range)+0.5);
	    int new_color = old_color+delta;
	    if (new_color>255)
		new_color = 255;
	    if (new_color<0)
		new_color = 0;
	    QColor color(new_color, new_color, new_color);
	    planet.height_map()->setPixel(x+h, y+k,color.rgb());
	}
    }
}


void CCanvas::mouseReleaseEvent(QMouseEvent *k) {
    
}



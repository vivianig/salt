varying vec2 vUv;
uniform sampler2D Texture;
varying vec3 normal, lightDir, eyeVec;

float h = texture2D(Texture, vUv)[0];

const vec4 grass = vec4(0.1411764705882353, 0.27058823529411763, 0.11764705882352941, 1.0);
const vec4 mountains = vec4(0.27058823529411763, 0.19215686274509805, 0.11372549019607843, 1.0);
const vec4 snow = vec4(0.6745098039215687, 0.7607843137254902, 0.8313725490196079, 1.0);

const float mountain_level = 0.6;
const float snow_level = 0.85;

vec4 get_color()
{
    if (h < mountain_level)
        return grass * h;
    else if (h < snow_level)
        return mountains * h;
    else
        return snow * h;
}

float get_shininess()
{
	if (h > 0.85)
		return 50.0;
	else
		return 10.0;
}

vec4 get_specular()
{
    if (h < mountain_level)
        return vec4(0.1);
    else if (h < snow_level)
        return vec4(0.0);
    else
        return vec4(0.);
}

void main (void)
{
    vec4 ambient = vec4(0.20, 0.20, 0.45, 0.0);
    vec4 diffuse = vec4(0.5, 0.5, 0.5, 0.0);
    vec4 specular = get_specular();
    float shininess = get_shininess();

    vec4 final_color = ( ambient + gl_LightSource[0].ambient ) * get_color();

    vec3 N = normalize(normal);
    vec3 L = normalize(lightDir);

    float lambertTerm = dot(N,L);

    if(lambertTerm > 0.0)
    {
        final_color += lambertTerm * gl_LightSource[0].diffuse * diffuse;

        vec3 E = normalize(eyeVec);
        vec3 R = reflect(-L, N);
        float specularity = pow( max(dot(R, E), 0.0), shininess );
        final_color += gl_LightSource[0].specular * specular * specularity;
    }

    gl_FragColor = final_color;
}
